import React, {useEffect} from 'react';
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import {useDispatch, useSelector} from "react-redux";
import Button from "../../components/Button";
import Container from "@material-ui/core/Container";
import PageHeader from "../../components/PageHeader";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import {getUsers} from "../../store/users/actions";
import Cookies from 'universal-cookie';
import {Link} from "react-router-dom";
import {getRrestaurant} from "../../store/restaurant/actions";
function UseDashboard() {
    const title = 'Resturant Details';
    const dispatch = useDispatch();
    const cookies = new Cookies();
    const {auth_user} = useSelector(state => state.users)
    useEffect(() => {
        dispatch(getUsers({id : cookies.get('user_id')}));
    }, []);
    const {allRestaurant} = useSelector(state => state.restaurant)
    useEffect(() => {
        dispatch(getRrestaurant());
    }, []);
    useEffect(() => {
        console.log("--------", allRestaurant);
    }, [allRestaurant,auth_user]);
    return (
        <Grid>
            <Grid align={'center'}>
                <Container maxWidth="lg">
                    <Paper style={{
                        padding: 30,
                        margin: '20px auto',
                    }} elevation={10}>
                        <Grid container justify={'space-between'} spacing={1} direction={'row'} alignItems={'center'}>
                            <Grid item>
                                <PageHeader title={title}/>
                            </Grid>
                            <Grid item>
                            </Grid>
                        </Grid>
                        <Grid container justify={'center'} spacing={1} direction={'row'} alignItems={'center'}>
                            <Grid item>
                                <Table fullWidth={true} size={'large'} align={'center'}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>#</TableCell>
                                            <TableCell align="center">Picture</TableCell>
                                            <TableCell align="center">Name</TableCell>
                                            <TableCell align="center">Address</TableCell>
                                            <TableCell align="center">description</TableCell>
                                            <TableCell align="center">Location</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            (allRestaurant) && allRestaurant.map(function (restaurant, index) {
                                                return (
                                                    <TableRow key={index}>
                                                        <TableCell align="center">{index + 1}</TableCell>
                                                        <TableCell align="center"><img
                                                            style={{
                                                                height: '200px',
                                                                width: '300px',
                                                            }}
                                                            src={restaurant.image.path}
                                                            alt={'no image'}
                                                        /></TableCell>
                                                        <TableCell align="center">{restaurant.name}</TableCell>
                                                        <TableCell align="center">{restaurant.address}</TableCell>
                                                        <TableCell align="center">{restaurant.description}</TableCell>
                                                        <TableCell align="center">
                                                            <Button
                                                                style={{textDecoration: 'none'}}
                                                            >
                                                                <Link to={'/maps'}>
                                                                    View Location
                                                                </Link>
                                                            </Button>
                                                        </TableCell>
                                                    </TableRow>
                                                )
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </Grid>
                        </Grid>
                    </Paper>
                </Container>
            </Grid>
        </Grid>
    );
}
export default UseDashboard;
