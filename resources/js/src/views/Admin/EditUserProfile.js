import React, {useEffect, useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "../../components/Button";

import {TextField} from "../../components/TextField";
import Grid from "@material-ui/core/Grid";
import validate from "validate.js";
import {addNotification} from "../../store/notification/actions";
import {notificationTypes} from "../../config";
import {useDispatch} from "react-redux";
import {editProfile} from "../../store/profile/actions";
import UploadUsertAvatar from "./UploadUserAvatar";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import {getUsers} from "../../store/users/actions";export default function EditUserProfile({data}) {
    const dispatch = useDispatch();
    const schema = {
        email: {
            presence: {allowEmpty: false, message: 'is required'},
            email: true
        },
        name: {
            presence: {allowEmpty: false, message: 'is required'}
        },
        phone: {
            presence: {allowEmpty: false, message: 'is required'},
            length: {
                maximum: 14,
                minimum: 10,
                message: 'is required size 10-14'
            },
            numericality: {
                onlyInteger: true
            },
        }
    };
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const [formState, setFormState] = useState({
        isValid: false,
        remember: false,
        values: {},
        touched: {},
        errors: {}
    });
    useEffect(() => {
        const errors = validate(formState.values, schema);
        setFormState(formState => ({
            ...formState,
            isValid: !errors,
            errors: errors || {}
        }));
    }, [formState.values]);
    useEffect(() => {
        setFormState(formState => ({
            ...formState,
            values: {
                id: data.id,
                name: data.name,
                email: data.email,
                phone: data.phone,
                image: data.profile_pic,
            },
            touched: {
                ...formState.touched,
                name: true,
                email: true,
                phone: true,
            }
        }));
    }, [data]);
    const handleChange = event => {
        event.persist();
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]: event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));
    };
    const handleSubmit = event => {
        event.preventDefault();
        if (!formState.isValid) {
            return;
        }
        dispatch(
            editProfile({...formState.values})
        ).then((response) => {
                if (response.data && response.data.status) {
                    console.log('response', response.data.user);
                    dispatch(addNotification({
                        message: 'Edit successful!',
                        type: notificationTypes.SUCCESS,
                    }));
                    handleClose();
                    dispatch(getUsers());
                }
            }
        );
    };
    const hasError = field =>
        !!(formState.touched[field] && formState.errors[field]);
    return (
        <div>
            <IconButton onClick={handleClickOpen}>
                <EditIcon/>
            </IconButton>
            <Dialog
                fullWidth={true}
                maxWidth={'lg'}
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"User Details"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <Grid container direction={'column'} spacing={1}>
                            <Grid item>
                                <UploadUsertAvatar id={formState.values.id} image={formState.values.image}/>
                            </Grid>
                            <Grid item>
                                <TextField autoComplete="off"
                                           error={hasError('name')}
                                           fullWidth
                                           helperText={hasError('name') ? formState.errors.name[0] : ' '}
                                           placeholder="Name"
                                           name="name"
                                           type="name"
                                           onChange={handleChange}
                                           value={formState.values.name}
                                />
                            </Grid>
                            <Grid item>
                                <TextField autoComplete="off"
                                           error={hasError('email')}
                                           fullWidth
                                           helperText={hasError('email') ? formState.errors.email[0] : ' '}
                                           placeholder="Email"
                                           name="email"
                                           type="email"
                                           onChange={handleChange}
                                           value={formState.values.email || ''}/>
                            </Grid>
                            <Grid item>
                                <TextField autoComplete="off"
                                           error={hasError('phone')}
                                           fullWidth
                                           helperText={hasError('phone') ? formState.errors.phone[0] : ' '}
                                           placeholder="Contact number"
                                           name="phone"
                                           type="text"
                                           onChange={handleChange}
                                           value={formState.values.phone || ''}/>
                            </Grid>
                        </Grid>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={handleSubmit}
                        disabled={!formState.isValid}
                    >
                        Edit
                    </Button>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
