import React, {useEffect} from 'react';
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {useDispatch, useSelector} from "react-redux";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {deleteUser, getUsers} from "../../store/users/actions";
import Avatar from "@material-ui/core/Avatar";
import EditUserProfile from "./EditUserProfile";
import {addNotification} from "../../store/notification/actions";
import {notificationTypes} from "../../config";
import DeleteDialogBox from "../../components/DeleteDialogBox/DeleteDialogBox";
function UserTab() {
    const dispatch = useDispatch();
    const {allusers} = useSelector(state => state.users)
    const [deleteId, setDeleteId] = React.useState(null);
    const [deleteOpen, setDeleteOpen] = React.useState(false);
    const handleDeleteOpen = () => {
        setDeleteOpen(true);
    };
    const handleDeleteClose = () => {
        setDeleteOpen(false);
    };
    const handleDeleteConformation = (e, id) => {
        e.preventDefault();
        setDeleteId(id);
        handleDeleteOpen();
    }
    useEffect(() => {
        dispatch(getUsers());
    }, []);
    useEffect(() => {
        console.log("-----",allusers,allusers.length);
    }, [allusers]);
    const handleDeleteUser = (e) => {
        e.preventDefault();
        dispatch(deleteUser({id: deleteId})).then(({response}) => {
            if (response.data.status) {
                dispatch(addNotification({
                    message: 'Delete Successfully',
                    type: notificationTypes.SUCCESS,
                }));
            } else {
                dispatch(addNotification({
                    message: 'Error',
                    type: notificationTypes.ERROR,
                }));
            }
        })
        handleDeleteClose();
        dispatch(getUsers());
    }
    return (
        <Table fullWidth={true} size={'large'} align={'center'}>
            <TableHead>
                <TableRow>
                    <TableCell>#</TableCell>
                    <TableCell align="center">Profile Picture</TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Email</TableCell>
                    <TableCell align="center">Phone Number</TableCell>
                    <TableCell align="center">Action</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {
                    (allusers.length !== 0) && allusers.map(function(user){
                        return(
                            <TableRow key={user.id}>
                                <TableCell align="center">{user.id} </TableCell>
                                <TableCell align="center">
                                    <Grid  justify={'center'} alignItems={'center'}>
                                        <Avatar     style={{
                                            height: '150px',
                                            width: '150px',
                                        }} alt="" src={user.profile_pic}/>
                                    </Grid>
                                </TableCell>
                                <TableCell align="center">{user.name}</TableCell>
                                <TableCell align="center">{user.email}</TableCell>
                                <TableCell align="center">{user.phone}</TableCell>
                                <TableCell align="center">
                                    <Grid container justify={'space-between'} spacing={1}
                                          direction={'column'} alignItems={'center'}>
                                        <Grid item>
                                            <IconButton onClick={(e) => {
                                                handleDeleteConformation(e, user.id)
                                            }} aria-label="delete">
                                                <DeleteIcon/>
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <IconButton aria-label="delete">
                                           <EditUserProfile data={user} />
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </TableCell>
                            </TableRow>
                        );
                    })
                }
            </TableBody>
            <DeleteDialogBox title={'Delete User Record'}
                             description={'Are sure you want to delete User record ?'}
                             open={deleteOpen}
                             handleClickOpen={handleDeleteOpen}
                             handleClose={handleDeleteClose}
                             handleDelete={handleDeleteUser}
            />
        </Table>
    );
}
export default UserTab;
