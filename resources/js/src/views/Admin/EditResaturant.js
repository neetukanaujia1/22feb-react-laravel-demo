import React, {useEffect, useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "../../components/Button";
import {TextField} from "../../components/TextField";
import Grid from "@material-ui/core/Grid";
import validate from "validate.js";
import EditIcon from '@material-ui/icons/Edit';
import {addNotification} from "../../store/notification/actions";
import {notificationTypes} from "../../config";
import {useDispatch} from "react-redux";
import IconButton from "@material-ui/core/IconButton";
import {editRestaurant, getRrestaurant} from "../../store/restaurant/actions";
import UploadRestaurantAvatar from "./UploadRestaurantAvatar";
export default function EditResaturant({data}) {
    const dispatch = useDispatch();
    const schema = {
        name: {
            presence: {allowEmpty: false, message: 'is required'}
        },
        description: {
            presence: {allowEmpty: false, message: 'is required'}
        },
        address: {
            presence: {allowEmpty: false, message: 'is required'}
        },
    };
    const handleEditRestaurant = (e) => {
        e.preventDefault();
        dispatch(editRestaurant({...formState.values})).then(({response}) => {
            if (response.data.status) {
                dispatch(addNotification({
                    message: 'Edit Successfully',
                    type: notificationTypes.SUCCESS,
                }));
                handleClose();
                dispatch(getRrestaurant());
            } else {
                dispatch(addNotification({
                    message: 'Error',
                    type: notificationTypes.ERROR,
                }));
            }
        })
    }
    useEffect(() => {
        console.log('data', {
            id: data.id,
            name: data.name,
            description: data.description,
            address: data.address,
        })
    }, [data]);
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
        if (data) {
            setFormState(formState => ({
                ...formState,
                values: {
                    id: data.id,
                    name: data.name,
                    description: data.description,
                    address: data.address,
                    image: data.image ? data.image.path : ''
                },
                touched: {
                    ...formState.touched,
                    name: true,
                    description: true,
                    address: true,
                    image: true,
                }
            }));
        }
    };
    const handleClose = () => {
        setOpen(false);
    };
    const [formState, setFormState] = useState({
        isValid: false,
        remember: false,
        values: {},
        touched: {},
        errors: {}
    });
    useEffect(() => {
        const errors = validate(formState.values, schema);
        setFormState(formState => ({
            ...formState,
            isValid: !errors,
            errors: errors || {}
        }));
    }, [formState.values]);
    useEffect(() => {
    }, [data]);
    const handleChange = event => {
        event.persist();
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]: event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));
    };
    const hasError = field =>
        !!(formState.touched[field] && formState.errors[field]);
    return (
        <div>
            <IconButton onClick={handleClickOpen} aria-label="delete">
                <EditIcon/>
            </IconButton>
            <Dialog
                fullWidth={true}
                maxWidth={'lg'}
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Edit Resturant"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <Grid container direction={'column'} spacing={1}>
                            <Grid item>
                                <UploadRestaurantAvatar image={formState.values.image} id={formState.values.id}/>
                            </Grid>
                            <Grid item>
                                <TextField autoComplete="off"
                                           error={hasError('name')}
                                           fullWidth
                                           helperText={hasError('name') ? formState.errors.name[0] : ' '}
                                           placeholder="Name"
                                           name="name"
                                           type="name"
                                           onChange={handleChange}
                                           value={formState.values.name}
                                />
                            </Grid>
                            <Grid item>
                                <TextField autoComplete="off"
                                           error={hasError('address')}
                                           fullWidth
                                           helperText={hasError('address') ? formState.errors.address[0] : ' '}
                                           placeholder="Address"
                                           name="address"
                                           type="text"
                                           onChange={handleChange}
                                           value={formState.values.address || ''}/>
                            </Grid>
                            <Grid item>
                                <TextField autoComplete="off"
                                           error={hasError('description')}
                                           fullWidth
                                           helperText={hasError('description') ? formState.errors.description[0] : ' '}
                                           placeholder="Description"
                                           name="description"
                                           type="text"
                                           onChange={handleChange}
                                           value={formState.values.description || ''}/>
                            </Grid>
                        </Grid>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={handleEditRestaurant}
                        disabled={!formState.isValid}
                    >
                        Edit
                    </Button>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
