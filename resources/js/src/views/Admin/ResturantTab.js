import React, {useEffect} from 'react';
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {useDispatch, useSelector} from "react-redux";
import {deleteRestaurant, getRrestaurant} from "../../store/restaurant/actions";
import DeleteIcon from '@material-ui/icons/Delete';
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Button from "../../components/Button";
import {Link} from "react-router-dom";
import EditResaturant from './EditResaturant'
import {addNotification} from "../../store/notification/actions";
import {notificationTypes} from "../../config";
import DeleteDialogBox from "../../components/DeleteDialogBox";
function ResturantTab() {
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [deleteId, setDeleteId] = React.useState(null);
    const {allRestaurant} = useSelector(state => state.restaurant)
    useEffect(() => {
        dispatch(getRrestaurant());
    }, []);
    useEffect(() => {
    }, [allRestaurant]);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const [deleteOpen, setDeleteOpen] = React.useState(false);
    const handleDeleteOpen = () => {
        setDeleteOpen(true);
    };
    const handleDeleteClose = () => {
        setDeleteOpen(false);
    };
    const handleDeleteConformation = (e, id) => {
        e.preventDefault();
        setDeleteId(id);
        handleDeleteOpen();
    }
        const handleDeleteRestaurant = (e) => {
        e.preventDefault();
        dispatch(deleteRestaurant({id: deleteId})).then(({response}) => {
            if (response.data.status) {
                dispatch(addNotification({
                    message: 'Delete Successfully',
                    type: notificationTypes.SUCCESS,
                }));

            } else {
                dispatch(addNotification({
                    message: 'Error',
                    type: notificationTypes.ERROR,
                }));
            }
            handleDeleteClose();
        })
        dispatch(getRrestaurant());
    }
    return (
        <Table fullWidth={true} size={'large'} align={'center'}>
            <TableHead>
                <TableRow>
                    <TableCell>#</TableCell>
                    <TableCell align="center">Picture</TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Address</TableCell>
                    <TableCell align="center">description</TableCell>
                    <TableCell align="center">Location</TableCell>
                    <TableCell align="center">Action</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {
                    (allRestaurant) && allRestaurant.map(function (restaurant, index) {
                        return (
                            <TableRow key={index}>
                                <TableCell align="center">{index + 1}</TableCell>
                                <TableCell align="center">
                                    <img
                                        style={{
                                            height: '200px',
                                            width: '300px',
                                        }}
                                        src={restaurant.image ? restaurant.image.path : ''}
                                        alt={'no image'}
                                    /></TableCell>
                                <TableCell align="center">{restaurant.name}</TableCell>
                                <TableCell align="center">{restaurant.address}</TableCell>
                                <TableCell align="center">{restaurant.description}</TableCell>
                                <TableCell align="center">
                                    <Button
                                        style={{textDecoration: 'none'}}
                                    >
                                        <Link to={'/maps'}>
                                            View Location
                                        </Link>
                                    </Button>
                                </TableCell>
                                <TableCell align="center">
                                    <Grid container justify={'space-between'} spacing={1}
                                          direction={'column'} alignItems={'center'}>
                                        <Grid item>
                                            <IconButton onClick={(e) => {
                                                handleDeleteConformation(e, restaurant.id)
                                            }} aria-label="delete">
                                                <DeleteIcon/>
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <EditResaturant data={restaurant}/>
                                        </Grid>
                                    </Grid>
                                </TableCell>
                            </TableRow>
                        )
                    })
                }
            </TableBody>
            <DeleteDialogBox title={'Delete Restaurant Record'}
                             description={'Are sure you want to delete Resturant record ?'}
                             open={deleteOpen}
                             handleClickOpen={handleDeleteOpen}
                             handleClose={handleDeleteClose}
                             handleDelete={handleDeleteRestaurant}
            />
        </Table>
    );
}
export default ResturantTab;
