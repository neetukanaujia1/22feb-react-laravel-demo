import React from 'react';
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import PageHeader from "../../components/PageHeader";
import AdminTabs from "./AdminTabs";

function AdminDashboard () {
const title ='Admin Dashboard';
        return (
            <Grid>
                <Grid align={'center'}>
                    <Container maxWidth="lg">
                        <Paper style={{
                            padding: 30,
                            margin: '20px auto',
                        }} elevation={10}>
                            <Grid container justify={'space-between'} spacing={1} direction={'row'} alignItems={'center'}>
                                <Grid item>
                                    <PageHeader title={title}/>
                                </Grid>
                                <Grid item>
                                </Grid>
                            </Grid>
                            <Grid container justify={'center'} spacing={1} direction={'row'} alignItems={'center'}>
                                <Grid item>
<AdminTabs />
                                </Grid>
                            </Grid>
                        </Paper>
                    </Container>
                </Grid>
            </Grid>
        );
}
export default AdminDashboard;
