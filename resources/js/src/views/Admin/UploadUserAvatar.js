import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from '@material-ui/styles';
import {Avatar} from '@material-ui/core';
import Grid from "@material-ui/core/Grid";
import Button from "../../components/Button";
import {notificationTypes} from "../../config";
import {addNotification} from "../../store/notification/actions";
import {removeAvatar, saveAvatar} from "../../store/profile/actions";
const useStyles = makeStyles(theme => ({
    fileInput: {
        display: 'none'
    },
    avatar: {
        height: 150,
        width: 150,
        cursor: 'pointer'
    },
    removeButton: {},
}));
const UploadUsertAvatar = ({image,id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const fileInputRef = React.useRef(null);
    const [avatar, setAvatar] = useState(image);
    useEffect(() => {
        setAvatar(image|| '');
    }, [image]);
    const avatarClickHandler = () => {
        fileInputRef.current.click();
    };
    const removeAvatarClickHandler = () => {
        dispatch(removeAvatar({id : id})).then(({response}) => {
            if (response && response.data && response.data) {
                setAvatar(null);
            }
            if (response.status){
                dispatch(addNotification({
                    message:  'Remove Successfully',
                    type: notificationTypes.SUCCESS,
                }));
            }
            else {
                dispatch(addNotification({
                    message:  'Network Error',
                    type: notificationTypes.ERROR,
                }));
            }
        });
    };
    const handleAvatarUpload = (e) => {
        const file = e.target.files[0];
        if (!file) return;
        const formData = new FormData();
        formData.append('file', file);
        formData.append('id',id);
        dispatch(saveAvatar(formData)).then(({response}) => {
            if (response && response.data && response.data) {
                setAvatar(response.data.user.profile_pic);
                dispatch(addNotification({
                    message: 'Saved',
                    type: notificationTypes.SUCCESS,
                }));
            }
        })
    };
    const imgUploadError = (e) => {
        setAvatar(null);
    };
    return (
        <Grid container spacing={2} direction={'column'} alignItems={'center'}>
            <Grid item lg={12} md={12} sm={12}>
                <input
                    accept='image/*'
                    className={classes.fileInput}
                    onChange={handleAvatarUpload}
                    ref={fileInputRef}
                    type="file"
                />
            </Grid>
            <Grid item lg={12} md={12} sm={12}>
                <Avatar

                    style={{
                        height: '200px',
                        width: '200px',
                    }}
                    className={classes.avatar}
                    imgProps={{
                        onError: imgUploadError
                    }}
                    onClick={avatarClickHandler}
                    src={avatar || '/images/no-avatar.png'}
                >
                </Avatar>
            </Grid>
            <Grid item lg={12} md={12} sm={12}>
                <Button
                    fullwidth
                    className={classes.removeButton}
                    disabled={!avatar}
                    onClick={removeAvatarClickHandler}
                >
                    Remove picture
                </Button>
            </Grid>
        </Grid>
    );
};
export default UploadUsertAvatar;
