import * as actionTypes from './types';const
    baseUrl = '/api/admin';
export const getRrestaurant= (params) => ({
    type: actionTypes.GET_RESTAURANT,
    request: {
        method: 'GET',
        url: `${baseUrl}/get-restaurant`,
        params
    }
});
export const getRrestaurantDetail= (params) => ({
    type: actionTypes.GET_RESTAURANT_DETAIL,
    request: {
        method: 'GET',
        url: `${baseUrl}/get-restaurant-detail`,
        params
    }
});
export const editRestaurant = (params) => ({
    type: actionTypes.EDIT_RESTAURANT,
    request: {
        method: 'POST',
        url: `${baseUrl}/edit-restaurant`,
        params
    }
});
export const deleteRestaurant = (data) => ({
    type: actionTypes.DELETE_RESTAURANT,
    request: {
        method: 'DELETE',
        url: `${baseUrl}/delete-restaurant`,
        data
    }
});
export const saveRestaurantAvatar = (data) => ({
    type: actionTypes.SAVE_RESTAURANT_AVATAR,
    request: {
        method: 'POST',
        url: `${baseUrl}/save-restaurant-avatar`,
        data
    }
});
export const removeRestaurantAvatar = (data) => ({
    type: actionTypes.REMOVE_RESTAURANT_AVATAR,
    request: {
        method: 'DELETE',
        url: `${baseUrl}/remove-restaurant-avatar`,
        data
    }
});
