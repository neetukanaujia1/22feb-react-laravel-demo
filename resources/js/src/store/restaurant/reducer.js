import {error, success} from 'redux-saga-requests';
import * as actionTypes from './types';
const initialState = {
    loading: false,
    error: false,
    allRestaurant: [],
};
const reducer = (state = initialState, action) => {
    const {type, data} = action;
    switch (type) {
        // Actions===============================================================
        case actionTypes.GET_RESTAURANT:
            return {...state, loading: true, error: null};
        //
        // Success===============================================================
        //
             case success(actionTypes.GET_RESTAURANT):
            return {...state, loading: false, allRestaurant: data.allRestaurant, error: null};
        //
        // Errors===============================================================
        //
        case error(actionTypes.GET_RESTAURANT):
        default:
            return state;
    }
};
export default reducer;
