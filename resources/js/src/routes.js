import React from "react";
import {Route, Switch} from "react-router-dom";
import AdminDashboard from "./views/Admin/AdminDashboard";
import AdminLogin from "./views/Admin/AdminLogin";
import Login from "./views/Users/Login";
import Register from './views/Users/Register'
import Maps from './views/Maps'
import UserDashboard from "./views/Users/UserDashboard";
const adminUrl = '/admin';
const ROUTES = [
    {
        path: "/",
        exact: true,
        component: Login
    },
    {
        path: "/maps",
        exact: true,
        component: Maps
    },
    {
        path: "/admin/login",
        exact: true,
        component: AdminLogin
    },
    {
        path: "/admin/dashboard",
        exact: true,
        component: AdminDashboard
    },
    {
        path: "/dashboard",
        exact: true,
        component: UserDashboard
    },
    {
        path: "/register",
        exact: true,
        component: Register
    },
    {
        component: () => <h1>Error 404</h1>,
    },
];
function RouteWithSubRoutes(route) {
    return (
        <Route
            path={route.path}
            exact={route.exact}
            render={props => <route.component {...props} routes={route.routes}/>}
        />
    );
}
export function RenderRoutes({routes}) {
    return (
        <Switch>
            {routes.map((route, i) => {
                return <RouteWithSubRoutes key={route.key} {...route} />;
            })}
            <Route component={() => <h1>Not Found!</h1>}/>
        </Switch>
    );
}
export default ROUTES;
