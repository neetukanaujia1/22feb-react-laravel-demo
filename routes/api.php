<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
#Admin
Route::group(['prefix' => 'admin'], function () {
    Route::get('/get-restaurant','Api\Admin\AdminController@getRestaurant');
    Route::get('/get-restaurant-detail','Api\Admin\AdminController@getRestaurantDetail');
    Route::post('/edit-restaurant','Api\Admin\AdminController@editRestaurant');
    Route::delete('/delete-restaurant','Api\Admin\AdminController@deleteRestaurant');
    Route::delete('/remove-restaurant-avatar','Api\Admin\AdminController@removeRestaurantAvatar');
    Route::post('/save-restaurant-avatar','Api\Admin\AdminController@saveRestaurantAvatar');
});
#Users UserController
Route::get('/get-users', 'Api\User\UserController@getUsers');
Route::delete('/delete-user', 'Api\User\UserController@deleteUser');
Route::post('/edit-users', 'Api\User\UserController@editUsers');
Route::delete('/delete-users', 'Api\User\UserController@deleteUsers');
Route::post('/profile/save-avatar', 'Api\User\UserController@saveAvatar');
Route::delete('/profile/remove-avatar', 'Api\User\UserController@removeAvatar');
Route::put('/profile/edit-profile', 'Api\User\UserController@editProfile');
#Auth
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\Auth\AuthController@login');
    Route::post('signup', 'Api\Auth\AuthController@signup');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'Api\Auth\AuthController@logout');
        Route::get('me', 'Api\Auth\AuthController@me');
        Route::post('confirm-email', 'Api\Auth\AuthController@confirmEmail');
    });    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'Api\Auth\AuthController@logout');
        Route::get('me', 'Api\Auth\AuthController@me');
    });
});
