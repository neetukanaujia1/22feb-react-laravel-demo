<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantImage extends Model
{
  protected $table='restaurant_image';
  protected $fillable =['path'];
}
