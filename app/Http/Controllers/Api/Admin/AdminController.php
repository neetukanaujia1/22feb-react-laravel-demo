<?php
namespace App\Http\Controllers\Api\Admin;
use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantImage;
use Illuminate\Http\Request;
class AdminController extends Controller
{
    public function getRestaurant(Request $request)
    {
        return response()->json([
            'allRestaurant' => Restaurant::all()->toArray()
        ]);
    }
    public function getRestaurantDetail(Request $request)
    {
        return response()->json([
            'RestaurantDetail' => Restaurant::where('id',$request->id)->toArray()
        ]);
    }
    public function editRestaurant(Request $request)
    {
        return response()->json([
            'status' => Restaurant::where('id',$request->id)->update([
                'name'=>$request->name,
                'description'=>$request->description,
                'address'=>$request->address,
            ]),
        ]);
    }
    public function deleteRestaurant(Request $request)
    {
        return response()->json([
            'status' => Restaurant::destroy($request->id),
        ]);
    }
    public function removeRestaurantAvatar(Request $request)
    {
        $res = Restaurant::find($request->id);
        if (file_exists(public_path($res->image->path))) {
            Restaurant::where('id', $request->id)->update(['restaurant_image_id' => null]);
            return response()->json([
                'status' =>  RestaurantImage::destroy($res->image->id),
            ]);
        } else {
            return response()->json([
                'status' => false,
                'msg' => 'File does not exists'
            ]);
        }
    }
    public function saveRestaurantAvatar(Request $request)
    {
        $request->validate([
            'file' => ['required', 'image', 'mimes:jpg,png,jpeg,gif', 'max:4096'],
        ]);
        $file = $request->file('file');
        $image_name = $file->getClientOriginalName();
        $filePath = '/restaurant-pic/' . $image_name;
        $request->file->move(public_path("/restaurant-pic"), $image_name);
        $created_restaurat = RestaurantImage::create([
            'path' => $filePath,
        ]);
        Restaurant::where('id', (int)$request->id)->update(['restaurant_image_id' => $created_restaurat->id]);
        return response()->json(compact('filePath'));
    }
}
