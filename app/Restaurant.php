<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table='restaurant';

    protected $with = ['image'];

    function image()
    {
        return $this->hasOne(RestaurantImage::class, 'id', 'restaurant_image_id');
    }



}
