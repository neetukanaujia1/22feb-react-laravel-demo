<?php

use App\Roles;
use App\RoleUser;
use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roles::insert([
          'name'=>'admin'
        ]);
        Roles::insert([
            'name'=>'user'
        ]);
    }
}
