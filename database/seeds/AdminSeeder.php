<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $public_img = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg'];
        User::insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'phone' => 1234567890,
            'role_id' => 1,
            'profile_pic' => '/profile-pic/' . $public_img[rand(0, 4)],
            'password' => Hash::make('admin@gmail.com'),
        ]);
        DB::table('oauth_access_tokens')->insert([
            'id' => 'acba6e554130c9897a54f8b8da2db68c5575a2f38a5d8d81e662ee3f07cd18822762db2fd69f6ac2',
            'user_id' => 1,
            'scopes' => 0,
            'revoked' => 0,
            'client_id' => 1,
            'name' => 'Personal Access Token',

        ]);
        DB::table('oauth_clients')->insert([
            'id' => 1,
            'name' => 'Laravel Personal Access Client',
            'secret' => '4hAwpj4RH2ayt5Uq0LUnZHLWvk5kPtgz2lhrPyuU',
            'redirect' => 'http://localhost',
            'personal_access_client' => 1,
            'password_client' => 0,
            'revoked' => 0,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('oauth_clients')->insert([
            'id' => 2,
            'name' => 'Laravel Password Grant Client',
            'secret' => 'SeP1RppUICvMOANAOnPrLJUD4xK3L7nWBRA48NCj',
            'redirect' => 'http://localhost',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => 0,
        ]);


        DB::table('oauth_personal_access_clients')->insert([
            'id' => 1,
            'client_id' => 1,
        ]);


    }
}
