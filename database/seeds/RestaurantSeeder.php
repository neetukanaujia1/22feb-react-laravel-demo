<?php
use App\Restaurant;
use App\RestaurantImage;
use Illuminate\Database\Seeder;
use Faker\Factory;
class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);   #runs forever
        $factory = Factory::create();
        $count = 5;
        $public_img = [1,2,3,4,5];
        $data=[
            1=>'/restaurant-pic/1.jpg',
           2=>'/restaurant-pic/2.jpg',
           3=>'/restaurant-pic/3.jpg',
           4=>'/restaurant-pic/4.jpg',
           5=>'/restaurant-pic/5.jpg',
        ];
        foreach ($data as $key=>$value){
            RestaurantImage::insert([
                'path'=> $value,
            ]);
        }
        for ($i = 0; $i < $count; $i++) {



            Restaurant::insert([
                'name' => $factory->name,
                'address' => $factory->address,
                'description' =>'description of resturant',
                'restaurant_image_id' => (int)$public_img[rand(0,4)],
            ]);
        }
    }
}
// 'address' => '/profile-pic/' . $public_img[rand(0,4)], /restaurant-pic/1.jpg
